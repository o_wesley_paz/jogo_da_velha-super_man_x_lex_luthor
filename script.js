var vez = 1;
var vencedor = "";
function casasIguais(a, b, c){
	var bgA = document.getElementById("casa"+a).style.backgroundImage;
	var bgB = document.getElementById("casa"+b).style.backgroundImage;
	var bgC = document.getElementById("casa"+c).style.backgroundImage;
	if ( (bgA == bgB) && (bgB == bgC) && (bgA != "none" && bgA != "")) {
		if(bgA.indexOf("1.png") >= 0)
			vencedor = "Super Man";
		else
			vencedor = "Lex Luthor";
		return true;
	}
	else{
		return false;
	}
}
var contador=0;
function verificarFimDeJogo(){
	var temVencedor=false;
	if((casasIguais(1, 2, 3)) || (casasIguais(4, 5, 6)) || (casasIguais(7, 8, 9))){
		temVencedor=true;
	}
	else if((casasIguais(1, 4, 7)) || (casasIguais(2, 5, 8)) || (casasIguais(3, 6, 9))){
		temVencedor=true;
	}
	else if((casasIguais(1, 5, 9)) || (casasIguais(3, 5, 7))){
		temVencedor=true;
	}
	if(temVencedor) {
		document.getElementById("resultado").innerHTML = "<h1>" + vencedor + " venceu!</h1>"
		var casas = document.getElementsByClassName("casa");
		for (var i=0; i < casas.length; i++)
			casas[i].onclick = null;
	}
	else if(contador==8) {
		document.getElementById("resultado").innerHTML = "<h1>Ninguém venceu, deu velha!</h1>";
	}
	contador++;
}
function reiniciarJogo() {
	location.reload();
}
function verificarCasa(casa) {
	var bg = document.getElementById(casa).style.backgroundImage;
	if(bg == "none" || bg == "")
	{
		var fig = "url(" + vez.toString() + ".png)";
		document.getElementById(casa).style.backgroundImage = fig;
		if(vez==1){
			vez=2;
		}
		else{
			vez=1;
		}
		verificarFimDeJogo();
	}
}